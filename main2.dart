void main() {
  List allWinners = [
    'FNB App',
    'Bookly' ,
    'Zapper' ,
    'Dstv Now' ,
    'IKhokha.com' ,
    'Shyft',
    'Cowa-bunga' ,
    'Naked' ,
    'EasyEquities',
    'Ambani Africa App' ,
  ];

  allWinners.sort();
  allWinners.forEach((app) {
    print(app);
  });

  print("Winners of 2017 and 2018 :\n ${allWinners[2]} and  ${allWinners[8]}");
  print('Total number of apps : ${allWinners.length}');
}
