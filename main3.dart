class AppWinners {
  late String appName;
  late String category;
  late int year;

  AppWinners(String this.appName, String this.category, int this.year);

  void display() {
    print("$year  ${appName.toUpperCase()} $category");
  }
}

void main() {
  List allWinners = [
    'FNB App',
    'Bookly',
    'Zapper',
    'Dstv Now',
    'IKhokha.com',
    'Shyft',
    'Cowa-bunga',
    'Naked',
    'EasyEquities',
    'Ambani Africa App',
  ];
  List allCat = [
    'Best consumer solution',
    'Most innovation solution',
    'Best microsoft platform solution',
    'Best consumer solution',
    'Best consumer solution',
    'Best Financial solution',
    'Best Enterprice solution',
    'Best Financial solution',
    "Best consumer solution",
    'Best educational solution',
  ];

  for (int i = 0; i < allCat.length; i++) {
    AppWinners appWinner = AppWinners(allWinners[i], allCat[i], 2012 + i);
    appWinner.display();
  }
}
